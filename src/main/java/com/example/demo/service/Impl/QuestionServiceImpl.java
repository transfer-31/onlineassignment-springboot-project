package com.example.demo.service.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.QuestionForm;
import com.example.demo.model.Questions;
import com.example.demo.model.Result;
import com.example.demo.model.Subject;
import com.example.demo.repository.QuestionsRepository;
import com.example.demo.service.QuestionService;
import com.example.demo.service.SubjectService;

@Service
public class QuestionServiceImpl implements QuestionService {
	@Autowired
	private QuestionsRepository questionRepo;
	
	@Autowired
	private SubjectService subjectService;
	
	
	

	@Override
	public void saveQuestion(Questions question) {
		// TODO Auto-generated method stub
		Subject sub=subjectService.getById(question.getSubjectId());
		question.setSubject(sub.getSubjectName());
		
		questionRepo.save(question);
	}

//----------------------------------------------------------------------------------------------------	
//	@Override
//	public List<Questions> getListOfQuestions(String sub) {
//		// TODO Auto-generated method stub
//
//		System.out.println("questiocheck" + questionRepo.findBySubject(sub));
//
//		System.out.println("AllQuestions check" + questionRepo.findAll());
//
//		System.out.println("check" + sub);
//
//		return questionRepo.findBySubject(sub);
//		// return null;
//	}
//------------------------------------------------------------------------------------------------------

	@Override
	public List<Questions> getListOfQuestions(Long subjectId) {
		// TODO Auto-generated method stub
		System.out.println("question--"+questionRepo.findBysubjectId(subjectId));
		return questionRepo.findBysubjectId(subjectId);
	}

	@Override
	public void deleteQuestion(Integer id) {

		questionRepo.deleteById(id);
	}

	@Override
	public List<Questions> findAll() {
		return questionRepo.findAll();
	}

	

}
