package com.example.demo.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Role;
import com.example.demo.repository.RoleRepository;
import com.example.demo.service.RoleService;
@Service
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	private RoleRepository roleRepo;

	@Override
	public void saveRole(Role role) {
		// TODO Auto-generated method stub
		roleRepo.save(role);
		
	}

	@Override
	public List<Role> getNonAdminRoles() {
		// TODO Auto-generated method stub
		List<Role> RolesList = roleRepo.findAll();
		List<Role> listOfNonAdminRoles = new ArrayList();

		for(Role role:RolesList) {	
			if(role.getRoleName().equalsIgnoreCase("Student")) {
				listOfNonAdminRoles.add(role);
		}
		}
		return listOfNonAdminRoles;
	}

	@Override
	public Role getRoleById(Integer id) {
		// TODO Auto-generated method stub
		return roleRepo.findById(id).get();

	}

	  

}
