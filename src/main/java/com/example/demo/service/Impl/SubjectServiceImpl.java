package com.example.demo.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Subject;
import com.example.demo.repository.SubjectRepository;
import com.example.demo.service.SubjectService;

@Service
public class SubjectServiceImpl implements SubjectService {
	
	@Autowired
	private SubjectRepository subjectRepo;

	@Override
	public void saveSubject(Subject subject) {
		
		subjectRepo.save(subject);
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Subject> getAllSubjects() {
		// TODO Auto-generated method stub
		return subjectRepo.findAll();
	}

	@Override
	public void deleteSubject(Long id) {
		subjectRepo.deleteById(id);
		
	}

	@Override
	public Subject getById(Long subjectId) {
		// TODO Auto-generated method stub
		return subjectRepo.findById(subjectId).get();
	}

}
