package com.example.demo.service.Impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Questions;
import com.example.demo.model.Quiz;
import com.example.demo.repository.QuizRepository;
import com.example.demo.service.QuizService;
@Service
public class QuizServiceImpl implements QuizService {
	
	@Autowired
	private QuizRepository quizRepo;

	@Override
	public void saveQuiz(Quiz quiz) {
		quizRepo.save(quiz);
	}

	@Override
	public List<Quiz> getAllQuizes() {
		return quizRepo.findAll();
	}

	@Override
	public List<Questions> getListOfQuestions(int quizId) {
		List<Questions> questions = new ArrayList();
		Quiz quiz = quizRepo.findById(quizId).get();

		
		for(Questions quest:quiz.getQuestions()) {

			questions.add(quest);
		}
		
		return questions;
	}

	@Override
	public void deleteQuizById(Integer id) {
		quizRepo.deleteById(id);
	}

	@Override
	public Quiz getQuizDetails(int quizId) {
		Quiz quiz = quizRepo.findById(quizId).get();
		return quiz;
	}

}
