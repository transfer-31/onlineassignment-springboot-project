package com.example.demo.service.Impl;


import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.QuestionForm;
import com.example.demo.model.Questions;
import com.example.demo.model.Result;
import com.example.demo.repository.ResultRepository;
import com.example.demo.service.QuestionService;
import com.example.demo.service.ResultService;

//import jakarta.servlet.http.HttpSession;
@Service
public class ResultServiceImpl implements ResultService {
	@Autowired
	private Result result;
	
	@Autowired
	private QuestionService questionService;
	
	@Autowired
	private ResultRepository resultrepo;
	
	@Autowired
	private HttpSession session;

	@Override
	public Result getResult(QuestionForm qForm) {
		result.setMarksObtained(calculateMarksScored(qForm));
		result.setTotalMarks(calculateTotalMarks(qForm));
//		result.setQuizName(qForm.getQuestions().get(0).getQuiz().getQuizName());
		String quizName= (String) session.getAttribute("quizName");
		result.setQuizName(quizName);

		result.setDateOfExam(qForm.getDate());
		return result;
	}
	
	

	private int calculateTotalMarks(QuestionForm qForm) {
		
		int totalMarks = 0;
		for (Questions question : qForm.getQuestions()) {
					totalMarks += question.getWeightage();

		}
		return totalMarks;
	}

	private int calculateMarksScored(QuestionForm qForm) {
		// TODO Auto-generated method stub
		int marksScored = 0;
		for (Questions question : qForm.getQuestions()) {
			if (question.getChoosenAns() != null) {
				if (question.getChoosenAns().equals(question.getCorrectAns())) {
					marksScored += question.getWeightage();
				}
			}

		}
		return marksScored;
	}



	@Override
	public void saveResult(Result result) {
		resultrepo.save(result);
				
	}



	@Override
	public List<Result> getStudentResult(Integer userId) {
		return resultrepo.findByStudentId(userId);
	}



	@Override
	public List<Result> getAllSubmissions() {
		// TODO Auto-generated method stub
		return resultrepo.findAll();
	}

}
