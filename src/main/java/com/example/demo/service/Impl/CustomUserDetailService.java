package com.example.demo.service.Impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.model.CustomUserDetail;
import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;


@Service
public class CustomUserDetailService implements UserDetailsService{

	@Autowired
	UserRepository userRepository;
	
//	@Override
//	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
////		System.out.println("auth service"+user);
//
//		User user = userRepository.findByEmail(email);
//		UserDetails details;
//		details=new CustomUserDetail(user);
////		user.orElseThrow(() -> new UsernameNotFoundException("User not found, please register"));
////		return user.map(CustomUserDetail::new).get();
//		return details;
//	}
	
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		Optional<User> user = userRepository.findUserByEmail(email);
		

		System.out.println("auth service"+user);
		user.orElseThrow(() -> new UsernameNotFoundException("User not found, please register"));
		return user.map(CustomUserDetail::new).get();
	}

}
