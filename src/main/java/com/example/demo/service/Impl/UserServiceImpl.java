package com.example.demo.service.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Role;
import com.example.demo.model.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
@Service
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private RoleRepository roleRepo;

	@Override
	public void saveUser(User user) {
		// TODO Auto-generated method stub
		userRepo.save(user);

	}

	@Override
	public User getUserByMail(String Mail) {
		// TODO Auto-generated method stub
		
		return userRepo.findByEmail(Mail);
	}

	

	

	@Override
	public User findByEmailAndPassword(String email, String password) {
		System.out.println(userRepo.findByEmailAndPassword(email,password));
		
		return userRepo.findByEmailAndPassword(email,password);
	}

	@Override
	public List<User> viewAllStudents() {
		List<User> allUsersList=userRepo.findAll();
		List<User> allStudentsList = new ArrayList();
		for(User user:allUsersList) {
			System.out.println("id"+user.getUserId());
			System.out.println("Role id"+user.getRoleId());
			System.out.println("role"+user.getRole());


			if(user.getRole().getRoleName().equalsIgnoreCase("Student")) {
				allStudentsList.add(user);
			}
		}
		return allStudentsList;
	}

	@Override
	public void deleteStudent(Integer id) {
		userRepo.deleteById(id);;		
	}
	

}
