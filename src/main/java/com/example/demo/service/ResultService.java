package com.example.demo.service;

import java.util.List;

import com.example.demo.model.QuestionForm;
import com.example.demo.model.Result;

public interface ResultService {

	Result getResult(QuestionForm qForm);

	void saveResult(Result result);

	List<Result> getStudentResult(Integer userId);

	List<Result> getAllSubmissions();


}
