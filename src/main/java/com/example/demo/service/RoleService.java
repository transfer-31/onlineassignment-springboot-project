package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.Role;

public interface RoleService {
	public void saveRole(Role role);
	public List<Role> getNonAdminRoles();
	
	public Role getRoleById(Integer id);



}
