package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.model.Role;
import com.example.demo.model.User;
public interface UserService {
	
	public void saveUser(User user);
	
	public User getUserByMail(String Mail);
	


	public User findByEmailAndPassword(String email, String password);

	public List<User> viewAllStudents();

	public void deleteStudent(Integer id);


}
