package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Subject;

public interface SubjectService {

	void saveSubject(Subject subject);

	List<Subject> getAllSubjects();

	void deleteSubject(Long id);

	Subject getById(Long subjectId);

}
