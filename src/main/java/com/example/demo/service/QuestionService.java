package com.example.demo.service;

import java.util.List;

import com.example.demo.model.QuestionForm;
import com.example.demo.model.Questions;
import com.example.demo.model.Result;

public interface QuestionService {

	void saveQuestion(Questions question);

	List<Questions> getListOfQuestions(Long subjectId);

	void deleteQuestion(Integer id);

	List<Questions> findAll();

	
}
