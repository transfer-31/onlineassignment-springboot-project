package com.example.demo.service;


import java.util.List;

import com.example.demo.model.Questions;
import com.example.demo.model.Quiz;

public interface QuizService {

	void saveQuiz(Quiz quiz);

	List<Quiz> getAllQuizes();

	List<Questions> getListOfQuestions(int quizId);

	void deleteQuizById(Integer id);

	Quiz getQuizDetails(int quizId);

}
