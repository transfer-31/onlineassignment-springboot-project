package com.example.demo.controller;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.QuestionForm;
import com.example.demo.model.Questions;
import com.example.demo.model.Quiz;
import com.example.demo.model.Result;
import com.example.demo.repository.QuestionsRepository;
import com.example.demo.service.QuestionService;
import com.example.demo.service.QuizService;
import com.example.demo.service.ResultService;

//import ch.qos.logback.core.model.Model;
//import jakarta.servlet.http.HttpSession;

@Controller
public class QuizController {
	@Autowired
	private QuestionService questionService;

	@Autowired
	private ResultService resultService;

	@Autowired
	private QuestionForm qForm;

	@Autowired
	private HttpSession session;

	@Autowired
	private QuizService quizService;
	
	
	
@GetMapping("/test/start/{quizId}")
public ModelAndView getQuiz(@PathVariable int quizId) {
	String role = (String) session.getAttribute("role");
	
	
	if (session.getAttribute("userId") != null && role.equalsIgnoreCase("student")) {
				
		List<Questions> QuestionsList = quizService.getListOfQuestions(quizId);
		Quiz quizDetails = quizService.getQuizDetails(quizId);
		

		session.setAttribute("quizName", quizDetails.getQuizName());
		
		ModelAndView mv = new ModelAndView("Quiz");
		mv.addObject("questions",QuestionsList);
		mv.addObject("qForm",qForm);
		mv.addObject("quizDetails",quizDetails);

		
		return mv;
	}
	ModelAndView mv = new ModelAndView("redirect:/student/StudentPortal");		
	return mv;
	
}

@PostMapping("/test/submitQuiz")
public ModelAndView submit( QuestionForm qForm) {
	String role = (String) session.getAttribute("role");
	System.out.println("quizsubmit"+qForm.getQuestions());
   

	if (session.getAttribute("userId") != null && role.equalsIgnoreCase("student")) {
		
		 Integer userId = (Integer) session.getAttribute("userId");
			String userName = (String) session.getAttribute("username");


	 ModelAndView mv = new ModelAndView("Result");
	 
		String examDate = getTodayDate();
		qForm.setDate(examDate);
		
	 Result result = resultService.getResult(qForm);	 
	 result.setStudentId(userId);
	 result.setStudentName(userName);
	 
	 resultService.saveResult(result);
	 
	 mv.addObject("result",result);
        
		return mv;
	}
	else {
		ModelAndView mv = new ModelAndView(" redirect:/showUserLoginForm");

		return mv;
	}
}



public static String getTodayDate() {
	
	 LocalDate today = LocalDate.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
		String formattedDate = today.format(formatter);
		return formattedDate;
	
}

}
