package com.example.demo.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.model.Questions;
import com.example.demo.model.Quiz;
import com.example.demo.model.Result;
//import com.example.demo.model.Role;
import com.example.demo.model.Subject;
import com.example.demo.model.User;
import com.example.demo.service.QuestionService;
import com.example.demo.service.QuizService;
import com.example.demo.service.ResultService;
import com.example.demo.service.RoleService;
import com.example.demo.service.SubjectService;
import com.example.demo.service.UserService;

//import ch.qos.logback.core.model.Model;
//import jakarta.servlet.http.HttpServletResponse;
//import jakarta.servlet.http.HttpSession;

@Controller
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	@Autowired
	private QuestionService questionService;

	@Autowired
	private HttpSession session;
	
	@Autowired
	private SubjectService subjectService;
	
	@Autowired
	private ResultService resultService;
	
	@Autowired
	private QuizService quizService;
	
	@Autowired   
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	// -------------------------------------------Home page------------------------------------------------//
	@GetMapping("/")
	public ModelAndView showHomePage() {
		ModelAndView mv = new ModelAndView("WelcomePage");

		return mv;
	}

//----------------------------------------------user ----------------------------------------------------------------//

	@GetMapping("showUserLoginForm")
	public ModelAndView showUserLoginForm() {
		ModelAndView mv = new ModelAndView("UserLogin");
		mv.addObject("User", new User());
		return mv;
	}
	
	

	@GetMapping("showUserRegistrationForm")
	public ModelAndView showUserRegistrationForm() {

		ModelAndView mv = new ModelAndView("UserRegistration");
		mv.addObject("User", new User());
		mv.addObject("roles", roleService.getNonAdminRoles());
		return mv;
	}

	@PostMapping(path = "/SaveUser")
	public ModelAndView registerUserAccount(@ModelAttribute("User") User user) {
		User existing = userService.getUserByMail(user.getEmail());
		if (existing != null) {
//			return "redirect:/showUserRegistrationForm?error";
			ModelAndView mv = new ModelAndView("redirect:/showUserRegistrationForm?error");
			return mv;
		}
		else {
	//		 String password = user.getPassword();
		//     user.setPassword(bCryptPasswordEncoder.encode(password));
			ModelAndView mv = new ModelAndView("redirect:/showUserRegistrationForm?success");
			userService.saveUser(user);

			return mv ;

		}
		
	}

	@PostMapping(path = "/loginUser")
	public ModelAndView loginUser(@ModelAttribute("User") User user) {
//		 String password = user.getPassword();
//	     user.setPassword(bCryptPasswordEncoder.encode(password));
//	     User userDetails = userService.getUserByMail(user.getEmail());
	     
//	     boolean isPasswordMatched = bCryptPasswordEncoder.matches(user.getPassword(), userDetails.getPassword());
	     
//	     System.out.println("pass bool"+isPasswordMatched);



		User userDetails = userService.findByEmailAndPassword(user.getEmail(), user.getPassword());

		if (userDetails != null) {
			session.setAttribute("userdetails", userDetails);

			session.setAttribute("username", userDetails.getFirstName() + " " + userDetails.getLastName());
			session.setAttribute("userId", userDetails.getUserId());
			session.setAttribute("role", userDetails.getRole().getRoleName());
			System.out.println("session role"+userDetails.getRole().getRoleName());
			if (userDetails.getRole().getRoleName().equalsIgnoreCase("Admin")) {

				session.setAttribute("AdminLogin", userDetails.getRole().getRoleName());

			}

			if (userDetails.getRole().getRoleName().equalsIgnoreCase("student")) {
				List<Quiz> quizes = quizService.getAllQuizes();			
				ModelAndView mv = new ModelAndView("redirect:/student/StudentPortal");
				mv.addObject("quizesList",quizes);

				return mv;

			} else if (userDetails.getRole().getRoleName().equalsIgnoreCase("Admin")) {
				List<Subject> subjects = subjectService.getAllSubjects();

				ModelAndView mv = new ModelAndView("redirect:/admin/AdminPortal");
				mv.addObject("subjectList", subjects);

				return mv;
			}

			else {
				ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm?roleError");

				return mv;
			}
		}

		else {
			ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm?invalidUserError");

			return mv;
		}

	}

//<----------------------------------logout------------------------------------------------------------------>
	@GetMapping(path = "/logout")
	public String logout(HttpServletResponse response) {
	    session.invalidate();
	    session.removeAttribute("username");
	    session.removeAttribute("userId");
	    session.removeAttribute("role");

		return "/logout";
	}

	// <----------------------------------Admin---------------------------------------------------------->

	@GetMapping("/admin/AdminPortal")
	public ModelAndView redirectToAdmin() {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
			List<Subject> subjects = subjectService.getAllSubjects();
			ModelAndView mv = new ModelAndView("AdminPortal");
			mv.addObject("subjectList", subjects);

			return mv;
		}
		else {
			ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

			return mv;
		}
	}
	
	@GetMapping("/admin/ViewAllStudents")
	public ModelAndView viewAllStudents(Model model) {

		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {

			ModelAndView mv = new ModelAndView("StudentList");

			List<User> students = userService.viewAllStudents();

			mv.addObject("students", students);
			return mv;
		} else {
			ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

			return mv;
		}

	}

	@GetMapping("/admin/student/delete")
	public String deleteStudent(@RequestParam("id") Integer id) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {

			userService.deleteStudent(id);
			return "redirect:/admin/ViewAllStudents";
		} else {

			return "redirect:/showUserLoginForm";
		}
	}
	
	@GetMapping("/admin/student/performance")
    public ModelAndView viewStudentResults(@RequestParam("id") Integer id) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
			ModelAndView mv = new ModelAndView("StudentPerformance");
			List<Result> listOfStudentResults = resultService.getStudentResult(id);
			mv.addObject("studentResults",listOfStudentResults);
        return mv;
		} else {
			ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

			return mv;
		}
    }
	
	
	@GetMapping("/admin/viewAllSubjectsAndQuizes")
    public ModelAndView viewAllSubjects() {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
		
		ModelAndView mv = new ModelAndView("SubjectList");

		List<Subject> subjects = subjectService.getAllSubjects();
		List<Quiz> quizesList = quizService.getAllQuizes();			

	
        mv.addObject("subjects", subjects);
        mv.addObject("quizes", quizesList);

        return mv;
		} else {
			ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

			return mv;
		}
	}
	
	@PostMapping("/admin/subject/delete")
    public String deleteSubject(@RequestParam("id") Long id) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {

			subjectService.deleteSubject(id);

        return "redirect:/admin/viewAllSubjectsAndQuizes";
		}
	 else {

		return "redirect:/showUserLoginForm";
	}
	}
			
	@GetMapping("/admin/subject/add")
	public ModelAndView showAddSubjectForm() {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
			ModelAndView mv = new ModelAndView("AddSubject");
			mv.addObject("subject",new Subject());
			
			return mv;
		}
		 else {
				ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

				return mv;
			}
	}
		
	
	
	@PostMapping("/admin/saveSubject")
	public ModelAndView saveSubject(Subject subject) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
		ModelAndView mv = new ModelAndView("AdminPortal");
		subjectService.saveSubject(subject);
		return mv;
		}
		 else {
				ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

				return mv;
			}

		
	}
	
	
	@GetMapping("/admin/Quiz/add")
	public ModelAndView showAddQuizForm() {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
			ModelAndView mv = new ModelAndView("AddQuiz");
			mv.addObject("quiz", new Quiz());
			mv.addObject("questions", questionService.findAll());

			
			return mv;
		}
		 else {
				ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

				return mv;
			}
		}
	
	@PostMapping("/admin/add-quiz")
    public String addQuiz(@ModelAttribute Quiz quiz) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
		
		quizService.saveQuiz(quiz);

        return "redirect:/admin/AdminPortal";
		}
		 else {
			 return	"redirect:/showUserLoginForm";

			}
	}
	
	@PostMapping("/admin/quiz/delete")
    public String deleteQuiz(@RequestParam("id") Integer id) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
			quizService.deleteQuizById(id);
        return "redirect:/admin/viewAllSubjectsAndQuizes";
		}
		 else {
			 return	"redirect:/showUserLoginForm";

			}
		}
	
	
	

	@GetMapping(path = "/admin/ShowAddQuestionForm")
	public ModelAndView ShowAddQuestionForm() {	
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {

			ModelAndView mv = new ModelAndView("AddQuestion");
			mv.addObject("Questions", new Questions());
			mv.addObject("Subjects",subjectService.getAllSubjects());

			return mv;
		}
		 else {
				ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

				return mv;
			}
		}
	
	


	@PostMapping(path = "/admin/SaveQuestion")
	public String saveQuestion(Questions question) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {

		questionService.saveQuestion(question);
		return "redirect:/admin/ShowAddQuestionForm?success";
		}
		 else {
			 return	"redirect:/showUserLoginForm";

			}

	}
	
	@GetMapping("/admin/allSubmittedQuizes")
    public ModelAndView viewAllSubmittedQuizes() {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
			ModelAndView mv = new ModelAndView("StudentPerformance");
			List<Result> listOfSubmissions = resultService.getAllSubmissions();
			mv.addObject("studentResults",listOfSubmissions);
        return mv;
		}
		 else {
				ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

				return mv;
			}
    }
	
	
	@GetMapping("/admin/test/viewQuestions/{subjectId}")
	public ModelAndView getQuiz(@PathVariable Long subjectId) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {
					
			List<Questions> QuestionsList = questionService.getListOfQuestions(subjectId);
			ModelAndView mv = new ModelAndView("QuestionsList");
			 mv.addObject("questions",QuestionsList);	
			 return mv;
		}
			 else {
					ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

					return mv;
				}
	}
	
	@GetMapping("/admin/question/delete")
    public String deleteQuestion(@RequestParam("id") Integer id) {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("ADMIN")) {

			questionService.deleteQuestion(id);
        return "redirect:/admin/AdminPortal";
		}
		 else {
			 return	"redirect:/showUserLoginForm";

			}
		}
		
	// <----------------------------------Student---------------------------------------------------------->

	@GetMapping("/student/StudentPortal")
	public ModelAndView redirectToStudent() {
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("student")) {
		
			List<Quiz> quizes = quizService.getAllQuizes();			
			
			ModelAndView mv = new ModelAndView("StudentPortal");
			mv.addObject("quizesList",quizes);
        return mv;
		}
		ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

		return mv;
	}
	
	@GetMapping("/student/getStudentPermormances")
	public ModelAndView getStudentPerformance() {
		
		String role = (String) session.getAttribute("role");
		if (session.getAttribute("userId") != null && role.equalsIgnoreCase("student")) {
			 Integer userId = (Integer) session.getAttribute("userId");

			ModelAndView mv = new ModelAndView("StudentPerformance");
			List<Result> listOfStudentResults = resultService.getStudentResult(userId);
			mv.addObject("studentResults",listOfStudentResults);
		
			return mv;

		}
		ModelAndView mv = new ModelAndView("redirect:/showUserLoginForm");

		return mv;
	}
}
