package com.example.demo.model;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class QuestionForm {
	
	private List<Questions> questions;
	
	private String date;

	public QuestionForm() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QuestionForm(List<Questions> questions) {
		super();
		this.questions = questions;
	}
	
	

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public List<Questions> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Questions> questions) {
		this.questions = questions;
	}
	
	


}
