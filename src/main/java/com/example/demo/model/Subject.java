package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

//import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
//import jakarta.persistence.GenerationType;
//import jakarta.persistence.Id;

@Entity
public class Subject {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long sId;
	private String subjectName;
	private String descrption;
	private Integer studentId;
	
	
	public Subject() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	

	public Subject(String subject, String descrption) {
		super();
		this.subjectName = subject;
		this.descrption = descrption;
	}





	public Subject(Long sId, String subject, String descrption, Integer studentId) {
		super();
		this.sId = sId;
		this.subjectName = subject;
		this.descrption = descrption;
		this.studentId = studentId;
	}





	public String getDescrption() {
		return descrption;
	}

	public void setDescrption(String descrption) {
		this.descrption = descrption;
	}

	public Integer getStudentId() {
		return studentId;
	}

	public void setStudentId(Integer studentId) {
		this.studentId = studentId;
	}

	

	public Long getsId() {
		return sId;
	}

	public void setsId(Long sId) {
		this.sId = sId;
	}





	public String getSubjectName() {
		return subjectName;
	}





	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	
	
	
}
