package com.example.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

//import jakarta.persistence.Column;
//import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
//import jakarta.persistence.GenerationType;
//import jakarta.persistence.Id;
//import jakarta.persistence.JoinColumn;
//import jakarta.persistence.Table;

@Entity
@Component
@Table(name = "results")
public class Result {
	
	    @Id
	    @GeneratedValue(strategy = GenerationType.IDENTITY)
	    private Integer id;
	    
	    private Integer studentId;
	    
	    private String quizName;

	    private int marksObtained;

	    private int totalMarks;
	    
	    private String dateOfExam;
	    
	    private String studentName;
	    
	    

		public Result() {
			super();
			// TODO Auto-generated constructor stub
		}


		
		public String getDateOfExam() {
			return dateOfExam;
		}

		public void setDateOfExam(String dateOfExam) {
			this.dateOfExam = dateOfExam;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public Integer getStudentId() {
			return studentId;
		}

		public void setStudentId(Integer studentId) {
			this.studentId = studentId;
		}

	
		public int getMarksObtained() {
			return marksObtained;
		}

		public void setMarksObtained(int marksObtained) {
			this.marksObtained = marksObtained;
		}

		public int getTotalMarks() {
			return totalMarks;
		}

		public void setTotalMarks(int totalMarks) {
			this.totalMarks = totalMarks;
		}

		public String getStudentName() {
			return studentName;
		}

		public void setStudentName(String studentName) {
			this.studentName = studentName;
		}



		public String getQuizName() {
			return quizName;
		}



		public void setQuizName(String quizName) {
			this.quizName = quizName;
		}
		
		
		
		
	    
	    

}
