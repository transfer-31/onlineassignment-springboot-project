package com.example.demo.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;

//import jakarta.persistence.CascadeType;
//import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
//import jakarta.persistence.GenerationType;
//import jakarta.persistence.Id;
//import jakarta.persistence.OneToMany;
//import jakarta.persistence.Table;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "quizes")
public class Quiz {
	
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		@Column(name = "quiz_id")
		private Integer quizId;
		private String quizName;
		
		
		@ManyToMany
		@JoinTable(name="quiz_questions",joinColumns=@JoinColumn(name="quiz_id"),
		inverseJoinColumns=@JoinColumn(name="q_id"))
		private List<Questions> questions;
		
//		@OneToMany(mappedBy = "quiz", cascade = CascadeType.ALL)
//	    private List<Questions> questions;

		public Quiz() {
			super();
			// TODO Auto-generated constructor stub
		}

		public Integer getQuizId() {
			return quizId;
		}

		public void setQuizId(Integer quizId) {
			this.quizId = quizId;
		}

		public String getQuizName() {
			return quizName;
		}

		public void setQuizName(String quizName) {
			this.quizName = quizName;
		}

		public List<Questions> getQuestions() {
			return questions;
		}

		public void setQuestions(List<Questions> questions) {
			this.questions = questions;
		}
		

		
	//	private QuestionForm questionslist;
		
		

}
