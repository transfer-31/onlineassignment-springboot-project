package com.example.demo.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class CustomUserDetail implements UserDetails{

	    private String userName;
	    private String password;
	    private List<GrantedAuthority> authorities;

	    public CustomUserDetail(User user) {
	        this.userName = user.getEmail();
	        this.password = user.getPassword();
	        
	        GrantedAuthority authority = new SimpleGrantedAuthority(user.getRole().getRoleName());
	        
	        List<GrantedAuthority> authorities = new ArrayList<>();
	        authorities.add(authority);
	        
	        this.authorities = authorities;


//	        Role role = user.getRole();
//	        List<GrantedAuthority> auth;
//	        auth.add(role.getRoleName());
	        
//	        this.authorities = Arrays.stream(user.getRole().getRoleName().split(","))
//                    .map(SimpleGrantedAuthority::new)
//                    .collect(Collectors.toList());	   
	        }

	    @Override
	    public Collection<? extends GrantedAuthority> getAuthorities() {
	    	System.out.println("authorities"+authorities);
	        return authorities;
	    }

	    @Override
	    public String getPassword() {
	    	System.out.println("password"+password);

	        return password;
	    }

	    @Override
	    public String getUsername() {
	    	System.out.println("userName"+userName);

	        return userName;
	    }

	    @Override
	    public boolean isAccountNonExpired() {
	        return true;
	    }

	    @Override
	    public boolean isAccountNonLocked() {
	        return true;
	    }

	    @Override
	    public boolean isCredentialsNonExpired() {
	        return true;
	    }

	    @Override
	    public boolean isEnabled() {
	        return true;
	    }
}
