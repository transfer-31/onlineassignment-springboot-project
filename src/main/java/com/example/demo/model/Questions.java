package com.example.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

//import jakarta.persistence.Entity;
//import jakarta.persistence.GeneratedValue;
//import jakarta.persistence.GenerationType;
//import jakarta.persistence.Id;
//import jakarta.persistence.JoinColumn;
//import jakarta.persistence.ManyToOne;
//import jakarta.persistence.OneToOne;
//import jakarta.persistence.Table;

@Entity
@Table(name = "questions")
public class Questions {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "q_id")
	private Integer qid;
	private String qname;
	private String option1;
	private String option2;
	private String option3;
	private String option4;
	private String correctAns;
	private String difficulty;
	private Integer weightage;
	private String subject;
	
	private Long subjectId;
	

	
	private String choosenAns;
	
	
	@ManyToMany(mappedBy="questions")
	private List<Quiz> quiz;
	
//	    @ManyToOne
//	    @JoinColumn(name = "quiz_Id", referencedColumnName = "quizId",insertable = false)
//	    private Quiz quiz;

	
	


	public Questions() {
		super();
		// TODO Auto-generated constructor stub
	}



	


	





	public List<Quiz> getQuiz() {
		return quiz;
	}












	public void setQuiz(List<Quiz> quiz) {
		this.quiz = quiz;
	}












	public Questions(String qname, String option1, String option2, String option3, String option4, String correctAns,
			String difficulty, Integer weightage, String subject, String choosenAns) {
		super();
		this.qname = qname;
		this.option1 = option1;
		this.option2 = option2;
		this.option3 = option3;
		this.option4 = option4;
		this.correctAns = correctAns;
		this.difficulty = difficulty;
		this.weightage = weightage;
		this.subject = subject;
		this.choosenAns = choosenAns;
	}


	public String getChoosenAns() {
		return choosenAns;
	}


	public void setChoosenAns(String choosenAns) {
		this.choosenAns = choosenAns;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public Integer getQid() {
		return qid;
	}


	public void setQid(Integer qid) {
		this.qid = qid;
	}


	public String getQname() {
		return qname;
	}


	public void setQname(String qname) {
		this.qname = qname;
	}


	public String getOption1() {
		return option1;
	}


	public void setOption1(String option1) {
		this.option1 = option1;
	}


	public String getOption2() {
		return option2;
	}


	public void setOption2(String option2) {
		this.option2 = option2;
	}


	public String getOption3() {
		return option3;
	}


	public void setOption3(String option3) {
		this.option3 = option3;
	}


	public String getOption4() {
		return option4;
	}


	public void setOption4(String option4) {
		this.option4 = option4;
	}


	public String getCorrectAns() {
		return correctAns;
	}


	public void setCorrectAns(String correctAns) {
		this.correctAns = correctAns;
	}


	public String getDifficulty() {
		return difficulty;
	}


	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}


	public Integer getWeightage() {
		return weightage;
	}


	public void setWeightage(Integer weightage) {
		this.weightage = weightage;
	}



	public Long getSubjectId() {
		return subjectId;
	}



	public void setSubjectId(Long subjectId) {
		this.subjectId = subjectId;
	} 
	
	
	
	
	
	
}
