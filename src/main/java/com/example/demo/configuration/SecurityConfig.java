package com.example.demo.configuration;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.example.demo.service.Impl.CustomUserDetailService;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Autowired
	CustomUserDetailService customUserDetailService;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		System.out.println("check secu");
		http
			.authorizeRequests()
			.antMatchers("/showUserRegistrationForm","/h2-console/**","/showUserLoginForm","/**","/SaveUser","/**").permitAll()
			.antMatchers("/admin**").hasRole("ADMIN")
			.antMatchers("/student**","/test").hasRole("STUDENT")
	        .anyRequest()
	        .authenticated()
	        .and()
	        .formLogin()
	        .loginPage("/showUserLoginForm")
            .permitAll()
	        .failureUrl("/")
	        .defaultSuccessUrl("/loginUser")
	        .usernameParameter("email")
	        .passwordParameter("password")
	        .and()
//	        .oauth2Login()
//	        .loginPage("/login")
//	        .successHandler(googleOAuth2SuccessHandler)
//	        .and()
	        .logout()
	        .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
	        .logoutSuccessUrl("/")
	        .invalidateHttpSession(true)
	        .deleteCookies("JSESSIONID")
	        .and()
	        .exceptionHandling()
	        .and()
	        .csrf()
	        .disable();
		http.headers().frameOptions().disable();	
	}
	
	 @Bean
	    public PasswordEncoder getPasswordEncoder() {
	        return NoOpPasswordEncoder.getInstance();
	    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception{
		System.out.println("check secu auth");

		auth.userDetailsService(customUserDetailService);
	}
	
	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder(12);
	}
	
}

