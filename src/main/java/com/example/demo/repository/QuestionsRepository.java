package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Questions;

@Repository
public interface QuestionsRepository extends JpaRepository<Questions, Integer> {
	
//	    @Query(value = "SELECT * FROM questions WHERE subject = ?1", nativeQuery = true)
//	    List<Questions> findBySubject(String subject);

		List<Questions> findBysubjectId(Long subjectId);

}
