package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.model.Role;
import com.example.demo.repository.RoleRepository;
import com.example.demo.service.RoleService;

@SpringBootApplication
public class OnlineAssesmentApplication {
	
	@Autowired
	private RoleService serv;

	public static void main(String[] args) {
		SpringApplication.run(OnlineAssesmentApplication.class, args);	
	}

}
